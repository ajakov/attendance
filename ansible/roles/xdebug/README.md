Xdebug
==============
Xdebug for vagrant box + PHPStorm + Chrome browser
    
##Browser setup
 1. install chrome addon xdebug-helper 
 2. go to addon options and choose PHPStorm for IDE (key: PHPSTORM)
 
##PHPStorm setup
 1. Connect PHPStorm to Vagrant :
 
    https://confluence.jetbrains.com/display/PhpStorm/Configuring+PhpStorm+to+work+with+a+VM 
    
    we already have a vagrant box, so start from step 2 and only do that!
    
    ```
    2. Configuring the remote PHP interpreter in the virtual box 
    ```   
    
 2. Add our remote server :
 
    1. Go to settings, under Languages  Frameworks | PHP and choose Servers.
    2. Press + and add parameters :
    
    ```
    Name - can be anything
    Host - ip adress or virtual host you are using
    Port : 80
    Debbuger : Xdebug
    Check "Use path mappings" and add "Absolute path on the server" only for your project root and map it to vagrant root (/var/www/application)
    ```
    
    3. Apply and Save
    
 3. Add debugger configuration :
 
    1. Go to Run | edit Configurations
    2. Press + and Add PHP Web Application with parameters: 
    
    ```
    Name - can be anything
    Server - choose server you created in last part
    Start Url :  /
    Browser : Chrome
    ```
    
    3. Apply and Save  
  
  4. Start debugger :
   
    go to Run | Debug and select configuration you created in last step, now your browser should open and in you Debugger console in PHPStorm you should see connected.