<?php
/**
 * Created by PhpStorm.
 * User: ajakov
 * Date: 2.12.18.
 * Time: 17.48
 */

namespace App\Services;

class HelperService
{

    /**
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @return array
     * @throws \Exception
     */
    public function generateDaysAndMonths(\DateTime $startDate, \DateTime $endDate) : array
    {
        $days = [];
        $months = [];

        $currentDate = clone $startDate;

        while ($currentDate <= $endDate) {
            $days[] = array(
                'day' => $currentDate->format('d'),
                'weekday' => $currentDate->format('D'),
                'full_date' => $currentDate->format('Y-m-d'),
            );


            $month = $currentDate->format('M');
            if (array_key_exists($month, $months)) {
                $months[$month]++;
            } else {
                $months[$month] = 1;
            }

            $currentDate->add(new \DateInterval('P1D'));
        }

        return [
          'days' => $days,
          'months' => $months,
        ];
    }

    public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }


}
