<?php
/**
 * Created by PhpStorm.
 * User: ajakov
 * Date: 29.12.18.
 * Time: 11.10
 */

namespace App\Services;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Security;

class UserService
{

    /**
     * @var Security
     */
    protected $security;

    /**
     * UserService constructor.
     */
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function getCurrentUserSelectedCompany()
    {
        return $this->security->getUser()->getSelectedCompany();
    }
}
