<?php
/**
 * Created by PhpStorm.
 * User: ajakov
 * Date: 29.12.18.
 * Time: 11.50
 */

namespace App\Services;


use App\Repository\AttendanceTagRepository;
use App\Services\UserService;

class AttendanceTagService
{

    protected $attendanceTagRepository;
    protected $userService;

    /**
     * AttendanceTagService constructor.
     */
    public function __construct(AttendanceTagRepository $attendanceTagRepository, UserService $userService)
    {
        $this->attendanceTagRepository = $attendanceTagRepository;
        $this->userService = $userService;
    }

    public function getTagsForCurrentCompany()
    {
        $currentCompany = $this->userService->getCurrentUserSelectedCompany();
        return $this->attendanceTagRepository->findBy(['company' => $currentCompany], ['position' => 'ASC']);
    }

    public function getTagIdByName($name)
    {
        $tags = $this->getTagsForCurrentCompany();
        foreach ($tags as $tag) {
            if($name == $tag->getName()) {
                return $tag->getId();
            }
        }
        return null;
    }

}

