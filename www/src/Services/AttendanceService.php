<?php


namespace App\Services;

use App\Entity\Attendance;
use App\Entity\Employee;
use App\Repository\AttendanceRepository;
use App\Repository\EmployeeRepository;
use Doctrine\ORM\EntityManagerInterface;

class AttendanceService
{
    private $attendanceRepository;
    private $employeeRepository;
    private $entityManager;

    public function __construct(AttendanceRepository $attendanceRepository, EmployeeRepository $employeeRepository, EntityManagerInterface $entityManager)
    {
        $this->attendanceRepository = $attendanceRepository;
        $this->employeeRepository = $employeeRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @param \DateTime $displayStartDate
     * @param \DateTime $displayEndDate
     * @return array
     * @throws \Exception
     */
    public function getSpecialAttendances(
        \DateTime $startDate,
        \DateTime $endDate
    ) {

        $attendances = $this->attendanceRepository->findSpecialBetweenDates($startDate, $endDate);
        return $attendances;
    }


}
