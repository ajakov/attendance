<?php


namespace App\Services;

use App\Entity\Attendance;
use App\Entity\Employee;
use App\Repository\AttendanceRepository;
use App\Repository\EmployeeRepository;
use Doctrine\ORM\EntityManagerInterface;

class EmployeeService
{
    private $attendanceRepository;
    private $employeeRepository;
    private $entityManager;

    public function __construct(AttendanceRepository $attendanceRepository, EmployeeRepository $employeeRepository, EntityManagerInterface $entityManager)
    {
        $this->attendanceRepository = $attendanceRepository;
        $this->employeeRepository = $employeeRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @param Employee $employee
     * @param \DateTime $displayStartDate
     * @param \DateTime $displayEndDate
     * @return array
     * @throws \Exception
     */
    public function getFormattedAttendances(
        Employee $employee,
        \DateTime $displayStartDate,
        \DateTime $displayEndDate
    ) {
        $now = new \DateTime();
        $tempDate = clone $displayStartDate;

        $attendances = [];

        $employeeAttendances = $this->attendanceRepository->findByEmployeeBetweenDates(
            $employee,
            $displayStartDate,
            $displayEndDate
        );
        $firstAttendance = reset($employeeAttendances);
        $firstAttendanceDate = $displayStartDate;
        if ($firstAttendance) {
            $firstAttendanceDate = $firstAttendance->getDate();
        }


        while ($tempDate < $firstAttendanceDate) {
            $attendances[] = [
                'date' => $tempDate->format('Y-m-d'),
                'status' => 0,
                'link' => '/attendance/new?date=' . $tempDate->format('Y-m-d') . '&employee=' . $employee->getId(),
            ];

            $tempDate->add(new \DateInterval('P1D'));
        }



        foreach ($employeeAttendances as $attendance) {
            while ($tempDate->format('Y-m-d') < $attendance->getDate()->format('Y-m-d')) {
                $tempStatus = 0;
                if ($tempDate > $now) {
                    $tempStatus = 'future';
                }

                $attendances[] = [
                    'date' => $tempDate->format('Y-m-d'),
                    'status' => $tempStatus,
                    'link' => '/attendance/new?date=' . $tempDate->format('Y-m-d') . '&employee=' . $employee->getId(),
                ];
                $tempDate->add(new \DateInterval('P1D'));
            }

            $attendances[] = [
                'date' => $attendance->getDate()->format('Y-m-d'),
                'status' => $attendance->getStatus(),
                'link' => '/attendance/' . $attendance->getId() . '/edit',
            ];

            $tempDate->add(new \DateInterval('P1D'));
        }

        while ($tempDate->format('Y-m-d') <= $now->format('Y-m-d') && $tempDate->format('Y-m-d') <= $displayEndDate->format('Y-m-d')) {
            if ($employee->getEndDate() && $employee->getEndDate()->format('Y-m-d') < $tempDate->format('Y-m-d')) {
                $tempDate->add(new \DateInterval('P1D'));
                continue;
            }
            $attendances[] = [
                'date' => $tempDate->format('Y-m-d'),
                'status' => 0,
                'link' => '/attendance/new?date=' . $tempDate->format('Y-m-d') . '&employee=' . $employee->getId(),
            ];
            $tempDate->add(new \DateInterval('P1D'));
        }

        return $attendances;
    }

    /**
     * @param int $id
     * @return Employee|null
     */
    public function loadById(int $id) : ?Employee
    {
        return $this->employeeRepository->findOneBy(['id' => $id]);
    }

    public function setAllAttendances($date, $status = 1) :void
    {
        $attendancesByDate = $this->attendanceRepository->findBy(['date' => new \DateTime($date)]);
        $employeeIds = [];
        foreach ($attendancesByDate as $attendance) {
            $employeeIds[] = $attendance->getEmployee()->getId();
        }

        $allEmployees = $this->employeeRepository->findAll();
        foreach ($allEmployees as $employee) {
            if (in_array($employee->getId(), $employeeIds)) {
                continue;
            }
            if ($employee->getEndDate() && $employee->getEndDate() > $date) {
                continue;
            }
            if ($employee->getStartDate() && $employee->getStartDate() < $date) {
                continue;
            }
            $attendance = new Attendance();
            $attendance->setEmployee($employee);
            $attendance->setDate(new \DateTime($date));
            $attendance->setStatus($status);
            $this->entityManager->persist($attendance);
        }
        $this->entityManager->flush();
    }

    public function setVacation(\DateTime $startDate, \DateTime $endDate, Employee $employee): void
    {
        while ($startDate <= $endDate) {
            if(in_array($startDate->format('w'), [0,6])) {
                $startDate->add(new \DateInterval('P1D'));
                continue;
            }
            $attendance = $this->attendanceRepository->findOneBy(['date' => $startDate, 'employee' => $employee]);
            if (!$attendance) {
                $attendance = new Attendance();
                $attendance->setEmployee($employee);
                $attendance->setDate($startDate);
            }
            if($attendance->getStatus() != Attendance::STATUS_HOLIDAY) {
                $attendance->setStatus(Attendance::STATUS_VACATION);
            }
            $this->entityManager->persist($attendance);
            $this->entityManager->flush();
            $attendance = null;
            $startDate->add(new \DateInterval('P1D'));
        }
    }
}
