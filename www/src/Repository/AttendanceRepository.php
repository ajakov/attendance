<?php

namespace App\Repository;

use App\Entity\Attendance;
use App\Entity\Employee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Attendance|null find($id, $lockMode = null, $lockVersion = null)
 * @method Attendance|null findOneBy(array $criteria, array $orderBy = null)
 * @method Attendance[]    findAll()
 * @method Attendance[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AttendanceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Attendance::class);
    }

    /**
     * @param $employee Employee
     * @param $startDate \DateTime
     * @param $endDate \DateTime
     * @return Attendance[] Returns an array of Attendance objects
     */
    public function findByEmployeeBetweenDates($employee, $startDate, $endDate) : array
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.employee = :employee')
            ->andWhere('a.date >= :start_date')
            ->andWhere('a.date <= :end_date')
            ->setParameter('employee', $employee)
            ->setParameter('start_date', $startDate)
            ->setParameter('end_date', $endDate)
            ->orderBy('a.date', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }


    public function findSpecialBetweenDates($startDate, $endDate) : array
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.date >= :start_date')
            ->andWhere('a.date <= :end_date')
            ->andWhere('a.status = 3 OR (a.comment IS NOT NULL AND a.comment <> \'\')')
            ->setParameter('start_date', $startDate)
            ->setParameter('end_date', $endDate)
            ->orderBy('a.date', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }


    public function findByEmployeeWithComments($employee) : array
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.employee = :employee')
            ->andWhere('a.comment <> :empty')
            ->andWhere('a.comment IS NOT NULL')
            ->setParameter('employee', $employee)
            ->setParameter('empty', '')
            ->orderBy('a.date', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    // /**
    //  * @return Attendance[] Returns an array of Attendance objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Attendance
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
