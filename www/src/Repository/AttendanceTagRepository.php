<?php

namespace App\Repository;

use App\Entity\AttendanceTag;
use App\Entity\EmployeeTag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AttendanceTag|null find($id, $lockMode = null, $lockVersion = null)
 * @method AttendanceTag|null findOneBy(array $criteria, array $orderBy = null)
 * @method AttendanceTag[]    findAll()
 * @method AttendanceTag[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AttendanceTagRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AttendanceTag::class);
    }

    // /**
    //  * @return AttendanceTag[] Returns an array of AttendanceTag objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AttendanceTag
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
