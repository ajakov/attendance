<?php

namespace App\API;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Attendance;

/**
* @Rest\RouteResource(
*     "Attendance",
*     pluralize=false
* )
*/
class AttendanceController extends FOSRestController implements ClassResourceInterface
{

    public function postAction(
        Request $request
    ) {

    }

}
