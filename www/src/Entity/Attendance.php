<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AttendanceRepository")
 */
class Attendance
{
    const STATUS_OFFICE = 1;
    const STATUS_REMOTE = 2;
    const STATUS_ABSENT = 3;
    const STATUS_VACATION = 4;
    const STATUS_HOLIDAY = 5;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Employee", inversedBy="attendances")
     * @ORM\JoinColumn(nullable=false)
     */
    private $employee;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=2048, nullable=true)
     */
    private $comment;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\AttendanceTag", inversedBy="attendances")
     */
    private $tags;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getEmployee(): ?Employee
    {
        return $this->employee;
    }

    public function setEmployee(?Employee $employee): self
    {
        $this->employee = $employee;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getComment() :?string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     * @return Attendance
     */
    public function setComment($comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return Collection|EmployeeTag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(EmployeeTag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(EmployeeTag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getDate()->format('d.m.Y') . ' - ' . $this->getEmployee()->__toString();
    }

    public function getIsOneOnOne()
    {
        foreach ($this->getTags() as $tag) {
            if ($tag->getName() == '1 on 1') {
                return true;
            }
        }
        return false;
    }

    public function getIsSick()
    {
        foreach ($this->getTags() as $tag) {
            if ($tag->getName() == 'sick') {
                return true;
            }
        }
        return false;
    }
}
