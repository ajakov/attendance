<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Integer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompanyRepository")
 */
class Company
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="companies")
     */
    private $accountOwner;

    /**
     * @ORM\Column(type="integer", name="default_status", nullable=true)
     */
    private $defaultStatus;

    /**
     * @ORM\Column(type="boolean", name="set_default_status_automatically", nullable=true)
     * @var boolean
     */
    private $setDefaultStatusAutomatically;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Employee", mappedBy="company")
     */
    private $employees;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AttendanceTag", mappedBy="company")
     */
    private $attendanceTags;

    public function __construct()
    {
        $this->employees = new ArrayCollection();
        $this->taxonomyVocabularies = new ArrayCollection();
        $this->attendanceTags = new ArrayCollection();
        $this->setDefaultStatus(Attendance::STATUS_OFFICE);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAccountOwner(): ?User
    {
        return $this->accountOwner;
    }

    public function setAccountOwner(?User $accountOwner): self
    {
        $this->accountOwner = $accountOwner;

        return $this;
    }

    /**
     * @return Collection|Employee[]
     */
    public function getEmployees(): Collection
    {
        return $this->employees;
    }

    public function addEmployee(Employee $employee): self
    {
        if (!$this->employees->contains($employee)) {
            $this->employees[] = $employee;
            $employee->setCompany($this);
        }

        return $this;
    }

    public function removeEmployee(Employee $employee): self
    {
        if ($this->employees->contains($employee)) {
            $this->employees->removeElement($employee);
            // set the owning side to null (unless already changed)
            if ($employee->getCompany() === $this) {
                $employee->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmployeeTag[]
     */
    public function getAttendanceTags(): Collection
    {
        return $this->attendanceTags;
    }

    public function addAttendanceTag(EmployeeTag $attendanceTag): self
    {
        if (!$this->attendanceTags->contains($attendanceTag)) {
            $this->attendanceTags[] = $attendanceTag;
            $attendanceTag->setCompany($this);
        }

        return $this;
    }

    public function removeAttendanceTag(EmployeeTag $attendanceTag): self
    {
        if ($this->attendanceTags->contains($attendanceTag)) {
            $this->attendanceTags->removeElement($attendanceTag);
            // set the owning side to null (unless already changed)
            if ($attendanceTag->getCompany() === $this) {
                $attendanceTag->setCompany(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return null\int
     */
    public function getDefaultStatus(): ?int
    {
        return $this->defaultStatus;
    }

    /**
     * @param mixed $defaultStatus
     * @return Company
     */
    public function setDefaultStatus($defaultStatus): Company
    {
        $this->defaultStatus = $defaultStatus;
        return $this;
    }

    /**
     * @return null|bool
     */
    public function isSetDefaultStatusAutomatically(): ?bool
    {
        return $this->setDefaultStatusAutomatically;
    }

    /**
     * @param bool $setDefaultStatusAutomatically
     * @return Company
     */
    public function setSetDefaultStatusAutomatically(bool $setDefaultStatusAutomatically): Company
    {
        $this->setDefaultStatusAutomatically = $setDefaultStatusAutomatically;
        return $this;
    }



}
