<?php


namespace App\Controller;


use App\Entity\Company;
use App\Entity\User;
use App\Form\CompanySettingsType;
use App\Form\UserSelectCompanyType;
use App\Services\AttendanceService;
use App\Services\EmployeeService;
use App\Services\HelperService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/profile", name="user_profile")
     */
    public function profileAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();


        $companyForm = $this->createForm(UserSelectCompanyType::class, $user);
        $companyForm->handleRequest($request);
        if ($companyForm->isSubmitted() && $companyForm->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$user` variable has also been updated
            /** @var User $updatedUser */
            $updatedUser = $companyForm->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($updatedUser);
            $entityManager->flush();

            $this->addFlash('success', 'Saved! Your selected company is now <b>' . $updatedUser->getSelectedCompany()->getName() . '</b>');

            return $this->redirectToRoute('user_profile');
        }

        $company = $user->getSelectedCompany();
        $companySettingsForm = $this->createForm(CompanySettingsType::class, $company);
        $companySettingsForm->handleRequest($request);
        if ($companySettingsForm->isSubmitted() && $companySettingsForm->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$user` variable has also been updated
            /** @var Company $updatedCompany */
            $updatedCompany = $companySettingsForm->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($updatedCompany);
            $entityManager->flush();

            $this->addFlash('success', 'Company data saved!');

            return $this->redirectToRoute('user_profile');
        }

        return $this->render('user/profile.html.twig', [
            'company_form' => $companyForm->createView(),
            'company_settings_form' => $companySettingsForm->createView(),
            'company' => $company,
        ]);
    }
}