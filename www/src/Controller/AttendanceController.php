<?php

namespace App\Controller;

use App\Entity\Attendance;
use App\Form\AttendanceShortType;
use App\Form\VacationType;
use App\Repository\AttendanceRepository;
use App\Services\EmployeeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/attendance")
 */
class AttendanceController extends AbstractController
{
    private $employeeService;

    public function __construct(EmployeeService $employeeService)
    {
        $this->employeeService = $employeeService;
    }


    /**
     * @Route("/", name="attendance_index", methods="GET")
     */
    public function index(AttendanceRepository $attendanceRepository): Response
    {
        return $this->render('attendance/index.html.twig', ['attendances' => $attendanceRepository->findAll()]);
    }

    /**
     * @Route("/new", name="attendance_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $attendance = new Attendance();
        if ($getDate = $request->query->get('date')) {
            $attendance->setDate(new \DateTime($getDate));
        }
        if ($getEmployeeId = $request->query->get('employee')) {
            $attendance->setEmployee($this->employeeService->loadById($getEmployeeId));
        }
        $form = $this->createForm(AttendanceShortType::class, $attendance);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($attendance);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }

        return $this->render('attendance/new.html.twig', [
            'attendance' => $attendance,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="attendance_show", methods="GET")
     */
    public function show(Attendance $attendance): Response
    {
        return $this->render('attendance/show.html.twig', ['attendance' => $attendance]);
    }

    /**
     * @Route("/{id}/edit", name="attendance_edit", methods="GET|POST")
     */
    public function edit(Request $request, Attendance $attendance): Response
    {
        $form = $this->createForm(AttendanceShortType::class, $attendance);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('homepage');
        }

        $form_view = $form->createView();

        return $this->render('attendance/edit.html.twig', [
            'attendance' => $attendance,
            'form' => $form_view,
        ]);
    }

    /**
     * @Route("/{id}", name="attendance_delete", methods="DELETE")
     */
    public function delete(Request $request, Attendance $attendance): Response
    {
        if ($this->isCsrfTokenValid('delete'.$attendance->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($attendance);
            $em->flush();
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/fill_all/{date}/{status}", name="attendance_fill_all", methods="GET")
     */
    public function fillAllByDateAndStatus(Request $request, $date, $status = 1)
    {
        $this->employeeService->setAllAttendances($date, $status);
        $referer = $request->headers->get('referer');
        if(empty($referer)) {
            return $this->redirectToRoute('homepage');
        }
        return $this->redirect($referer);
    }


}
