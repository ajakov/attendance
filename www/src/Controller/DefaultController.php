<?php


namespace App\Controller;

use App\Entity\Attendance;
use App\Entity\User;
use App\Form\PublicHollidayType;
use App\Services\AttendanceService;
use App\Services\AttendanceTagService;
use App\Services\EmployeeService;
use App\Services\HelperService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Employee;
use Symfony\Component\HttpFoundation\Request;
use App\Form\VacationType;
use Symfony\Component\Validator\Constraints\Date;

/**
 * @author Aleksandar Jakovljevic <aj@computerrock.com>
 */
class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(Request $request, EmployeeService $employeeService, HelperService $helperService, AttendanceService $attendanceService, AttendanceTagService $attendanceTagService)
    {
        /** @var User $currentUser */
        $currentUser = $this->getUser();

        $entityManager = $this->getDoctrine();
        $employees = $entityManager->getRepository("App\Entity\Employee")->findAll();
        $employeesView = [];

        $startDate = $request->query->get('startDate');
        $endDate = $request->query->get('endDate');

        $displayStartDate = $startDate ? new \DateTime($startDate) : new \DateTime('-1 months');
        $displayEndDate = $endDate ? new \DateTime($endDate) : new \DateTime();

        /* @var $employee Employee */
        foreach ($employees as $employee) {
            if ($employee->getEndDate() && $employee->getEndDate() < $displayStartDate) {
                continue;
            }

            if ($employee->getStartDate() > $displayEndDate) {
                continue;
            }

            $currentEmployeeView = [
                'id' => $employee->getId(),
                'firstName' => $employee->getFirstName(),
                'lastName' => $employee->getLastName(),
                'attendances' => $employeeService->getFormattedAttendances(
                    $employee,
                    $displayStartDate,
                    $displayEndDate
                ),
            ];

            $employeesView[] = $currentEmployeeView;
        }

        $calendarLabels = $helperService->generateDaysAndMonths($displayStartDate, $displayEndDate);

        $special_events = $attendanceService->getSpecialAttendances($displayStartDate, $displayEndDate);

        return $this->render('default/index.html.twig', [
            'employees' => $employeesView,
            'days' => $calendarLabels['days'],
            'months' => $calendarLabels['months'],
            'special_events' => $special_events,
            'dateRangePickerStartDate' => $displayStartDate->format('Y-m-d'),
            'dateRangePickerEndDate' => $displayEndDate->format('Y-m-d'),
            'defaultStatus' => $currentUser->getSelectedCompany()->getDefaultStatus(),
            'oneOnOneTagId' => $attendanceTagService->getTagIdByName('1 on 1'),
            'sickTagId' => $attendanceTagService->getTagIdByName('sick'),
        ]);
    }

    /**
     * @Route("/vacation", name="vacation", methods="GET|POST")
     */
    public function vacation(Request $request, EmployeeService $employeeService)
    {
        $form = $this->createForm(VacationType::class);
        $form->handleRequest($request);

        $formData = [];

        if ($form->isSubmitted()) {
            $formData = $form->getData();

            $employeeService->setVacation($formData['startDate'], $formData['endDate'], $formData['employee']);

            return $this->redirectToRoute('homepage');
        }

        return $this->render('vacation/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/public_holliday", name="public_holliday", methods="GET|POST")
     */
    public function publicHolliday(Request $request, EmployeeService $employeeService)
    {
        $form = $this->createForm(PublicHollidayType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $formData = $form->getData();
            $employeeService->setAllAttendances($formData['date']->format('Y-m-d'), 5);

            return $this->redirectToRoute('homepage');
        }

        return $this->render('public_holliday/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
