<?php

namespace App\Controller;

use App\Entity\AttendanceTag;
use App\Form\AttendanceTagType;
use App\Repository\AttendanceTagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tags/attendance")
 */
class AttendanceTagController extends AbstractController
{
    /**
     * @Route("/", name="attendance_tag_index", methods="GET")
     */
    public function index(AttendanceTagRepository $attendanceTagRepository): Response
    {
        return $this->render('attendance_tag/index.html.twig', ['attendance_tags' => $attendanceTagRepository->findAll()]);
    }

    /**
     * @Route("/new", name="attendance_tag_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $attendanceTag = new AttendanceTag();
        $form = $this->createForm(AttendanceTagType::class, $attendanceTag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($attendanceTag);
            $em->flush();

            return $this->redirectToRoute('attendance_tag_index');
        }

        return $this->render('attendance_tag/new.html.twig', [
            'attendance_tag' => $attendanceTag,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="attendance_tag_show", methods="GET")
     */
    public function show(AttendanceTag $attendanceTag): Response
    {
        return $this->render('attendance_tag/show.html.twig', ['attendance_tag' => $attendanceTag]);
    }

    /**
     * @Route("/{id}/edit", name="attendance_tag_edit", methods="GET|POST")
     */
    public function edit(Request $request, AttendanceTag $attendanceTag): Response
    {
        $form = $this->createForm(AttendanceTagType::class, $attendanceTag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('attendance_tag_index', ['id' => $attendanceTag->getId()]);
        }

        return $this->render('attendance_tag/edit.html.twig', [
            'attendance_tag' => $attendanceTag,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="attendance_tag_delete", methods="DELETE")
     */
    public function delete(Request $request, AttendanceTag $attendanceTag): Response
    {
        if ($this->isCsrfTokenValid('delete'.$attendanceTag->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($attendanceTag);
            $em->flush();
        }

        return $this->redirectToRoute('attendance_tag_index');
    }
}
