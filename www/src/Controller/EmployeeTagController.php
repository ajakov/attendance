<?php

namespace App\Controller;

use App\Entity\EmployeeTag;
use App\Form\EmployeeTagType;
use App\Repository\EmployeeTagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tags/employee")
 */
class EmployeeTagController extends AbstractController
{
    /**
     * @Route("/", name="employee_tag_index", methods="GET")
     */
    public function index(EmployeeTagRepository $employeeTagRepository): Response
    {
        return $this->render('employee_tag/index.html.twig', ['employee_tags' => $employeeTagRepository->findAll()]);
    }

    /**
     * @Route("/new", name="employee_tag_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $employeeTag = new EmployeeTag();
        $form = $this->createForm(EmployeeTagType::class, $employeeTag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($employeeTag);
            $em->flush();

            return $this->redirectToRoute('employee_tag_index');
        }

        return $this->render('employee_tag/new.html.twig', [
            'employee_tag' => $employeeTag,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="employee_tag_show", methods="GET")
     */
    public function show(EmployeeTag $employeeTag): Response
    {
        return $this->render('employee_tag/show.html.twig', ['employee_tag' => $employeeTag]);
    }

    /**
     * @Route("/{id}/edit", name="employee_tag_edit", methods="GET|POST")
     */
    public function edit(Request $request, EmployeeTag $employeeTag): Response
    {
        $form = $this->createForm(EmployeeTagType::class, $employeeTag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('employee_tag_index', ['id' => $employeeTag->getId()]);
        }

        return $this->render('employee_tag/edit.html.twig', [
            'employee_tag' => $employeeTag,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="employee_tag_delete", methods="DELETE")
     */
    public function delete(Request $request, EmployeeTag $employeeTag): Response
    {
        if ($this->isCsrfTokenValid('delete'.$employeeTag->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($employeeTag);
            $em->flush();
        }

        return $this->redirectToRoute('employee_tag_index');
    }
}
