<?php
/**
 * Created by PhpStorm.
 * User: ajakov
 * Date: 23.12.18.
 * Time: 13.21
 */

namespace App\EventListener;

use App\Entity\AttendanceTag;
use App\Entity\Employee;
use App\Services\HelperService;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AutoFiller
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var HelperService
     */
    private $helperService;

    /**
     * AutoFiller constructor.
     */
    public function __construct(TokenStorageInterface $tokenStorage, HelperService $helperService)
    {
        $this->tokenStorage = $tokenStorage;
        $this->helperService = $helperService;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if ($entity instanceof Employee) {
            $this->postPersistEmployee($args);
        }

        if ($entity instanceof  AttendanceTag) {
            $this->postPersistTag($args);
        }
    }

    /**
     * postPersist callback for Employee entity
     * @param LifecycleEventArgs $args
     */
    public function postPersistEmployee(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity->getOrderPosition()) {
            $entity->setOrderPosition($entity->getId());
        }
        if (!$entity->getCompany()) {
            $entity->setCompany($this->tokenStorage->getToken()->getUser()->getSelectedCompany());
        }
        $entityManager->persist($entity);
        $entityManager->flush();
    }


    /**
     * postPersist callback for AttendanceTag and EmployeeTag entity
     * @param LifecycleEventArgs $args
     */
    public function postPersistTag(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity->getPosition()) {
            $entity->setPosition($entity->getId());
        }

        if (!$entity->getCompany()) {
            $entity->setCompany($this->tokenStorage->getToken()->getUser()->getSelectedCompany());
        }

        if (!$entity->getSlug()) {
            $entity->setSlug($this->helperService->slugify($entity->getName()));
        }

        $entityManager->persist($entity);
        $entityManager->flush();
    }
}
