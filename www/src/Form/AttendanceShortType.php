<?php

namespace App\Form;

use App\Entity\Attendance;
use App\Entity\Employee;
use App\Services\AttendanceTagService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use App\Form\Type\HiddenDateTimeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Services\UserService;
use App\Entity\AttendanceTag;
use App\Repository\AttendanceTagRepository;
use Doctrine\ORM\EntityRepository;

class AttendanceShortType extends AbstractType
{
    protected $attendanceTagService;

    /**
     * AttendanceType constructor.
     */
    public function __construct(AttendanceTagService $attendanceTagService)
    {
        $this->attendanceTagService = $attendanceTagService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', HiddenType::class)
            ->add('status', HiddenType::class)
            ->add('employee', EntityType::class, array(
                'class' => Employee::class,
                'label' => false,
                'attr' => ['class' => 'd-none']
            ))
            ->add('comment', TextareaType::class, array(
                'required' => false,
                'attr' => ['class' => 'form-control']
            ))
            ->add('tags', EntityType::class, array(
                'multiple' => true,
                'expanded' => true,
                'required' => false,
                'class' => AttendanceTag::class,
                'choices' => $this->attendanceTagService->getTagsForCurrentCompany(),
                'choice_attr' => function ($choiceValue, $key, $value) {
                    // adds a class like attending_yes, attending_no, etc
                    return ['class' => 'attendance_tag_'.strtolower($key)];
                },
            ))
        ;

        $builder->get('date')->addModelTransformer(new DateTimeToStringTransformer());
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Attendance::class,
        ]);
    }
}
