<?php

namespace App\Form;

use App\Entity\Attendance;
use App\Entity\Company;
use App\Entity\Employee;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class CompanySettingsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entity = $builder->getData();
        $builder
            ->add('name',
                TextType::class,
                [
                    'label' => 'Name',
                    'attr' => ['class' => 'form-control'],
                    'label_attr' => ['class' => 'col-form-label'],
                ]
            )
            ->add('defaultStatus',
                ChoiceType::class,
                [
                    'label' => 'Default status',
                    'choices' => [
                        'Office' => Attendance::STATUS_OFFICE,
                        'Remote' => Attendance::STATUS_REMOTE,
                        'Absent' => Attendance::STATUS_ABSENT,
                        'Vacation' => Attendance::STATUS_VACATION,
                        'Holiday' => Attendance::STATUS_HOLIDAY,
                    ],
                    'attr' => ['class' => 'form-control'],
                    'label_attr' => ['class' => 'col-form-label'],
                ]
            )
            ->add('setDefaultStatusAutomatically',
                CheckboxType::class,
                [
                    'required' => false,
                    'label' => 'Set default status automatically',
                    'attr' => ['class' => 'form-control'],
                    'label_attr' => ['class' => 'col-form-label'],
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Company::class,
        ]);
    }
}
