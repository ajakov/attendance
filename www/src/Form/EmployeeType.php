<?php

namespace App\Form;

use App\Entity\Employee;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class EmployeeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, array(
                'attr' => ['class' => 'form-control'],
                'label_attr' => ['class' => 'col-3 col-form-label'],
            ))
            ->add('lastName', TextType::class, array(
                'attr' => ['class' => 'form-control'],
                'label_attr' => ['class' => 'col-3 col-form-label'],
            ))
            ->add('position', TextType::class, array(
                'attr' => ['class' => 'form-control'],
                'label_attr' => ['class' => 'col-3 col-form-label'],
            ))
            ->add('startDate', DateType::class, array(
                'widget' => 'single_text',
                // prevents rendering it as type="date", to avoid HTML5 date pickers
                'html5' => false,
                // adds a class that can be selected in JavaScript
                'attr' => ['class' => 'js-datepicker form-control', 'data-toggle' => 'datepicker'],
                'label_attr' => ['class' => 'col-3 col-form-label'],
            ))
            ->add('endDate', DateType::class, array(
                'widget' => 'single_text',
                // prevents rendering it as type="date", to avoid HTML5 date pickers
                'html5' => false,
                'required' => false,
                // adds a class that can be selected in JavaScript
                'attr' => ['class' => 'js-datepicker form-control', 'data-toggle' => 'datepicker'],
                'label_attr' => ['class' => 'col-3 col-form-label'],
            ))
            ->add('orderPosition', HiddenType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Employee::class,
        ]);
    }
}
