<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181222180329 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE employee_employee_tag (employee_id INT NOT NULL, employee_tag_id INT NOT NULL, INDEX IDX_2439EA7E8C03F15C (employee_id), INDEX IDX_2439EA7E93E3A784 (employee_tag_id), PRIMARY KEY(employee_id, employee_tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employee_tag (id INT AUTO_INCREMENT NOT NULL, company_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) DEFAULT NULL, position INT DEFAULT NULL, INDEX IDX_A0CC13E979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE attendance_attendance_tag (attendance_id INT NOT NULL, attendance_tag_id INT NOT NULL, INDEX IDX_78A56188163DDA15 (attendance_id), INDEX IDX_78A5618810F87815 (attendance_tag_id), PRIMARY KEY(attendance_id, attendance_tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE attendance_tag (id INT AUTO_INCREMENT NOT NULL, company_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) DEFAULT NULL, position INT DEFAULT NULL, INDEX IDX_5960621979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE employee_employee_tag ADD CONSTRAINT FK_2439EA7E8C03F15C FOREIGN KEY (employee_id) REFERENCES employee (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE employee_employee_tag ADD CONSTRAINT FK_2439EA7E93E3A784 FOREIGN KEY (employee_tag_id) REFERENCES employee_tag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE employee_tag ADD CONSTRAINT FK_A0CC13E979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE attendance_attendance_tag ADD CONSTRAINT FK_78A56188163DDA15 FOREIGN KEY (attendance_id) REFERENCES attendance (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE attendance_attendance_tag ADD CONSTRAINT FK_78A5618810F87815 FOREIGN KEY (attendance_tag_id) REFERENCES attendance_tag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE attendance_tag ADD CONSTRAINT FK_5960621979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE user ADD selected_company_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649A01B5E35 FOREIGN KEY (selected_company_id) REFERENCES company (id)');
        $this->addSql('CREATE INDEX IDX_8D93D649A01B5E35 ON user (selected_company_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE employee_employee_tag DROP FOREIGN KEY FK_2439EA7E93E3A784');
        $this->addSql('ALTER TABLE attendance_attendance_tag DROP FOREIGN KEY FK_78A5618810F87815');
        $this->addSql('DROP TABLE employee_employee_tag');
        $this->addSql('DROP TABLE employee_tag');
        $this->addSql('DROP TABLE attendance_attendance_tag');
        $this->addSql('DROP TABLE attendance_tag');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649A01B5E35');
        $this->addSql('DROP INDEX IDX_8D93D649A01B5E35 ON user');
        $this->addSql('ALTER TABLE user DROP selected_company_id');
    }
}
