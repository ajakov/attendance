/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.css');
require('@chenfengyuan/datepicker/dist/datepicker.css');
require('../css/daterangepicker.css');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
var $ = require('jquery');
// this "modifies" the jquery module: adding behavior to it
// the bootstrap module doesn't export/return anything
require('bootstrap');

var datepicker = require('@chenfengyuan/datepicker');
var daterangepicker = require('../js/daterangepicker.min');

$(document).ready(function(){
    $('.js-datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });

    $('.employee-table-raw-attendance').hover(
        function() {
            var employeeId = $(this).data('employee');
            var attendanceDate = $(this).data('attendancedate');
            $('span[data-employee=' + employeeId + ']').addClass('highlighted');
            $('span[data-attendancedate=' + attendanceDate + ']').addClass('highlighted');
        },
        function() {
            var employeeId = $(this).data('employee');
            var attendanceDate = $(this).data('attendancedate');
            $('span[data-employee=' + employeeId + ']').removeClass('highlighted');
            $('span[data-attendancedate=' + attendanceDate + ']').removeClass('highlighted');
        }
    );

    $('.attendance-color-chooser-box').click(function (event) {
        $('.attendance-color-chooser-box').removeClass('selected');
        $(this).addClass('selected');
        var status = $(this).data('status');
        $('#attendance_short_status').val(status);
    });

    $('input[name="daterange"]').daterangepicker({
        opens: 'left',
        locale: {
            format: 'YYYY-MM-DD'
        }
    }, function(start, end, label) {
        window.location.href = '/?startDate=' + start.format('YYYY-MM-DD') + '&endDate=' + end.format('YYYY-MM-DD');
    });


});


