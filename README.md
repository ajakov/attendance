PHP7 / nginx / MySQL environment
==============

Vagrant + ansible development environment.

##Installation
 1. Put `Vagrantfile` and `ansible` directory in project root.
 2. Add new host in your local machine `hosts` file:
    1. open `/etc/hosts`
    ```
     sudo nano /etc/hosts
     ```
    2. Add this line to the end of the file:
    ```
     192.168.87.3   dev.vagrant
     ```
     `ctrl+x` to save the file
 
 3. Open up the terminal and run `vagrant up`
    1. If this is a **Symfony** project, make sure you add:
    ```
    # Relocated cache and logs folder for vagrant
    public function getCacheDir()
    {
        if (in_array($this->environment, array('dev', 'test'))) {
            return '/var/www/cache/' .  $this->environment;
        }
        return parent::getCacheDir();
    }

    public function getLogDir()
    {
        if (in_array($this->environment, array('dev', 'test'))) {
            return '/var/www/logs';
        }
        return parent::getLogDir();
    }
    ```
    to the `/app/AppKernel.php` file.
 4. That's it!

##Usage
Project is now available on `http://dev.vagrant`

All Symfony console commands should be issued from guest machine using ` vagrant ssh`.

##Directory structure 
Project folder is shared between host (your Mac) and guest (Vagrant VM) machine and is available on `/var/www/application` directory on guest machine.

Due to Vagrant's shared folder read/write speed limitations, Symfony's `cache` and `log` directories aren't shared and therefore they are relocated to `/var/www/cache` and `/var/www/logs`.

## Front-end assets

Working tutorials: 

node.js: https://websiteforstudents.com/install-the-latest-node-js-and-nmp-packages-on-ubuntu-16-04-18-04-lts/

yarn: https://linuxize.com/post/how-to-install-yarn-on-ubuntu-18-04/

working with assets using encore: https://symfony.com/doc/current/frontend/encore/installation.html